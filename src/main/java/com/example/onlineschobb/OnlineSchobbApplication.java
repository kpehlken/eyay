package com.example.onlineschobb;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class OnlineSchobbApplication {

	private DatabaseConnector connection = new DatabaseConnector("127.0.0.1", 3306, "schobb", "root", "password");

	public static void main(String[] args) {
		SpringApplication.run(OnlineSchobbApplication.class, args);
	}

	@CrossOrigin
	@GetMapping(path="/products", produces="application/json")
	public String product() {
		connection.executeStatement("SELECT * FROM product");
		QueryResult r = connection.getCurrentQueryResult();

		JSONArray jsonResponse = new JSONArray();
		for(int i = 0; i < r.getData().length; i++) {
			JSONObject json = new JSONObject();
			json.put("id", r.getData()[i][0]);
			json.put("name", r.getData()[i][1]);
			json.put("price", r.getData()[i][2]);
			json.put("image", r.getData()[i][3]);
			jsonResponse.put(json);
		}
		return jsonResponse.toString();
	}

	@CrossOrigin
	@PostMapping(path="/cart/quantity", consumes = "application/json", produces = "application/json")
	public String updateQuantity(@RequestHeader("userid") int id, @RequestBody String data) {
		
		JSONObject response = new JSONObject();
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> map = null;

		try {
			map = mapper.readValue(data, Map.class);
		} catch (Exception e) {
			System.out.println(e);
			response.put("success", "false");
			return response.toString();
		} 
		
		String oldQuantityStr = map.get("oldQuantity") + "";
		int oldQuantity = Integer.parseInt(oldQuantityStr);

		String amountStr = map.get("amount") + "";
		int amount = Integer.parseInt(amountStr);

		if(oldQuantity == 1 && amount == -1 || oldQuantity <= 0 ) {
			// Delete Row
			connection.executeStatement("DELETE FROM cart WHERE userid='" + id + "' AND productid='" + map.get("productid") + "';");
		} 
	
		System.out.println("Update QNT of ID: " + map.get("productid") + " By " + amount);	// DEV

		String sql = "UPDATE cart SET quantity=quantity+" + map.get("amount") + " WHERE productid='" + map.get("productid") + "'";
		connection.executeStatement(sql);


		response.put(("success"), "true");
		return response.toString();
	}

	@CrossOrigin
	@GetMapping(path="/cart", produces="application/json")
	public String getCart(@RequestHeader("userid") int id) {

		JSONObject response = new JSONObject();

		// Get all ids of products stored with userid: id
		connection.executeStatement("SELECT DISTINCT productid, quantity FROM cart WHERE userid='" + id + "'");
		QueryResult res = connection.getCurrentQueryResult();

		// Check if Cart is empty
		System.out.println("Cart Length: " + res.getData().length);
		if(res.getData().length == 0) {
			response.put("length", 0);	// Notify Frontend that Cart is empty
			return response.toString();
		}

		JSONObject products = new JSONObject();
		String[][] productDetails = res.getData();	// Stores all productIDs

		// Iterate through all productIDs
		for(int i = 0; i < productDetails.length; i++) {
			// Get product with productID 
			connection.executeStatement("SELECT * FROM product WHERE productid='" + productDetails[i][0] + "'");
			QueryResult r = connection.getCurrentQueryResult();
			// Iterate through all indexes and add to products Object and Add to response 
			JSONObject product = new JSONObject();
			product.put("id", r.getData()[0][0]);
			product.put("name", r.getData()[0][1]);
			product.put("price", r.getData()[0][2]);
			product.put("image", r.getData()[0][3]);
			product.put("quantity", productDetails[i][1]);
			products.put(Integer.toString(i), product);	// Add to Response for Frontend
		}
		response.put("products", products);
		return response.toString();
	}

	@CrossOrigin
	@PostMapping(path="/cart/add", consumes = "application/json", produces = "application/json")
	public String addToCart(@RequestBody String data) {
		ObjectMapper mapper = new ObjectMapper();
		JSONObject response = new JSONObject();
		Map<String,Object> map = null;

		try {
			map = mapper.readValue(data, Map.class);
		} catch (Exception e) {
			System.out.println(e);
			response.put("success", "false");
			return response.toString();
		} 

		String sql = "INSERT INTO cart (userid, productid, quantity) VALUES ('" + map.get("userid") + "','" + map.get("productid") + "','1')";
		connection.executeStatement(sql);

		response.put("success", "true");
		return response.toString();
	}

	@CrossOrigin
	@PostMapping(path="/register", consumes = "application/json", produces = "application/json")
	public String register(@RequestBody String data) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> map = null;
		try {
			map = mapper.readValue(data, Map.class);
		} catch (Exception e) {
			System.out.println(e);
		} 
		// Perform INSERT INTO Statement
		String sql = "INSERT INTO user (email, password) VALUES ('" + map.get("email") + "','" + map.get("password") + "')";
		connection.executeStatement(sql);
		
		// Perform SELECT Statement to get UserId
		connection.executeStatement("SELECT userid FROM user WHERE email='" + map.get("email") + "'");
		QueryResult res = connection.getCurrentQueryResult();

		JSONObject json = new JSONObject();
		json.put("success", "true");
		json.put("id", res.getData()[0][0]);
		return json.toString();
	}

	@CrossOrigin
	@PostMapping(path="/login", consumes = "application/json", produces = "application/json")
	public String login(@RequestBody String data) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> map = null;
		try {
			map = mapper.readValue(data, Map.class);
		} catch (Exception e) {
			System.out.println(e);
		} 

		String sql = "SELECT userid, password FROM user WHERE email='" + map.get("email") + "'"; 
		connection.executeStatement(sql);

		QueryResult res = connection.getCurrentQueryResult();

		JSONObject failResponse = new JSONObject();
		failResponse.put("success", "false");

		if(res == null) {
			System.out.println("Fail at DB Lookup");	// DEV
			return failResponse.toString();
		}

		String password = res.getData()[0][1].toString();
		String userid = res.getData()[0][0];

		if(!password.equals(map.get("password"))) {
			System.out.println("Fail at Password Check");	// DEV
			return failResponse.toString();
		}

		
		JSONObject json = new JSONObject();
		json.put("success", "true");
		json.put("id", userid);
		return json.toString();
	}
}
   